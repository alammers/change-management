## Change management process

These issues should help us understand how an adjustment or change to the way we currently do things will impact processes, systems, and employees within the organization. Please make sure to include all stakeholders or teams/departments affected by this change in this issue and be sure to detail in the description how this will impact them.

#### Please use this template to propose a change to a current system GitLab uses internally
* [ ] Check the [tech stack](https://docs.google.com/spreadsheets/d/1mTNZHsK3TWzQdeFqkITKA0pHADjuurv37XMuHv12hDU/edit#gid=0) for the business and technical owners of the system being proposed and please assign appropriately.
* [ ] Please make sure to share this issue with all relevant stakeholders or teams/departments.
* [ ] Mention `@gitlab-com/gl-security/secops` in comments for security review
* [ ] Admin: Review the requested change and comment on feasibility
* [ ] Security: Review for potential security issues
* [ ] Mention `@gitlab-com/business-ops/bizops-bsa`
* [ ] If this change is related to adding a new application to Okta, please include a comment with the names of the Okta App Admins for the tool you are rolling out.

### Please list the system and how it currently works

* [Example: Update zoom to auto record meetings]

### Use this section to describe the proposed change and the business need/benefit for applying the change

* [Example: Make a change to admin settings in zoom to have every meeting started auto record, then the person hosting can turn off if needed. This change follows our tranparancy value, etc.]

### Impact this change would have on business

* [Would allow everyone a chance to record and upload meetings for transparency. Would require everyone to take an extra step in turning off though when the meeting starts if the intention is not to record.]

### List any handbook pages where this change would require an update/addition to the handbook 

* [Example link.]

### Plan to announce change to GitLab team members 
(We recommend announcing any change *at least* in the #company_announcements Slack channel)

* [ ]  [Handbook?]
* [ ]  [Slack?]
* [ ]  [Company/Department calls?]
* [ ]  [No announcements required?]


/label  ~"Change Management"
