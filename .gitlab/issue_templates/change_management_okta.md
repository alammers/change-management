Use this Template to provide details of Applications that need to be integrated into Okta. Applications that are added to Okta may require some configuration of the Application by a user with Administrative rights. 

# Business Technology Change Management - Okta Applications
Please make sure to include all stakeholders or teams/departments affected by this change in this issue and be sure to detail in the description how this will impact them.

## Change management process

### When is this change needed by?
{+Please overwrite this text with the Month, Day, and Year of when this change is to be implemented into Production use}

#### Please use this template to track change(s) to a current third-party system GitLab uses internally
* [ ] Admin responsible for configuration: {+Please tag the system administrator(s) that will be responsible for working with IT on this project+}
* [ ] Please make sure to share this issue with all stakeholders or teams/departments that access this system.
* [ ] Mention `@gitlab-com/gl-security/appsec` in comments for [application security reviews](https://about.gitlab.com/handbook/engineering/security/#internal-application-security-reviews) 
* [ ] Please include a comment with the names of the system admins who will become Okta App Admins for this tool.
* [ ] Please tag the `@gl-people-exp` when the okta integration is complete so the offboarding template can be updated

### Approval Flow
- [ ] Business Owner Approval: {+Please tag the Business Owner(s) and request they check off this box with their approval+}
- [ ] Technical Owner Approval: {+Please tag the Technical Owner(s) and request they check off this box with their approval+}

### What type of change is this?

* [ ] Updating Okta settings due to Vendor Update
* [ ] Adding the system to Okta
* [ ] Access Request Role Based Entitlement Access Modification
   * [ ] Additional approval from a manager and/or director from the department the role belongs to. [`add team member handle here`]
* [ ] Removing a system from Okta (if you are offboarding the system, please complete a [Tech Stack Offboarding issue](https://gitlab.com/gitlab-com/business-ops/Business-Operations/-/issues/new?issuable_template=offboarding_tech_stack) and link it to this issue)

### Testing Procedures

* [Clear detailed documentation of how the change will be tested (ie Rollout in Sandbox first). If testing is not applicable, detailed explanation of why testing is not required.]

* [ ] Yes this change requires testing
   * [ ] [link to testing issue]
* [ ] No this change does not require testing

### Change backout procedures

* [Example: revert applied changes back to previous version level of "x", or restore the initial configuration file to the switch or the firewall]

### List any handbook pages where this change would require an update to the handbook

* [Example link.]


## Okta configuration process (only for adding new systems)

### Application 

(Name of Application)

### Purpose of Application 

(What's the application used for in GitLab? Only needs to be brief)

### URL

(Application's URL for login access)

### Does it support SAML/OAuth?

(Please check in the Application's administrative settings if it can support SAML or OAuth. If you cannot find anything, mark this as Unknown and we can research it)

### Does it support Automated provisioning?

(Most SAML-supported Applications can support an automated provisioning model. If unknown, we can discover this)


### Administrative Model (eg. Admin portal, elevated administrator roles etc) :

(How is the application administered? Does it use a seperate admin portal or admin roles within existing accounts)

### Technical Owner/Administrator(s) :

(Person who owns the application, or has admin rights and authority to change configuration)

### Business Owner(s) :

(Financial Approver and/or change approver, in the case that changes to the Application setup need approval)

### Users/Departments :

(List the users of the Application, by Department, Team or Group)

### Are users sharing accounts? 

(If so, name all shared accounts and which users/groups are to be granted access to these accounts)

### Do users access this Application just through a website, or is there also Phone/PC Applications?

(Include Mobile or MacOS Apps that we may need to include in testing)

### Other Details

(Include anything else that may be special or unique about the Application)

___
___

## Do Not Edit Below

/cc @pkaldis

Business Technology Change Management: @kxkue

/label  ~"Change Management"
