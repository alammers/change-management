## Change Management

This project will be used to document and track GitLab's Change Management and Third Party Change Management requests. Refer to the Change Management and Third Part Change Management handbook pages for when a Change Management issue is required. 

For additional history of the change management issues, please refer to [IT-Ops Issue Tracker](https://gitlab.com/gitlab-com/business-ops/itops/issue-tracker/-/tree/master/zzz_DEPRECATED) deprecated folder. 

#### Useful Links

* [Engineering Change Management Handbook Page](https://about.gitlab.com/handbook/engineering/infrastructure/change-management/)
* [Business Technology Change Management Handbook Page](https://about.gitlab.com/handbook/business-ops/business-technology-change-management/)

### Change Request Types

**Standard**

A **standard change** is a pre-authorized change that is low risk, relatively common and follows a specified procedure or work instruction.
* A standard change is one that is frequently implemented, has repeatable implementation steps, and has a proven history of success.
* Standard changes have to go through the change management process.
* **They require a peer review and Impacted Team(s) Management approval.**
  * Approval by Management that is responsible for the particular system.

**Comprehensive**

A **comprehensive change** is *high risk, high impact,* or has a *more complex procedure.*
* All changes to **financially significant applications** also are considered comprehensive due to the type of systems that they affect and the potential impact that could occur if there is an issue.
* **Infrastructure changes** are also considered comprehensive.
* **They require peer review, Impacted Team(s) Management approval, Business Owner approval, and Head of IT Approval.**

**Emergency**

An **emergency change** follows the same approval process as comprehensive.
* It can be entered for approval after the change has been implemented in production.
* Emergency changes are intended to be used only where there is an immediate critical need to correct an operational or security issue that is preventing users from working or transactions to not be processed or processed incorrectly.
* **They require review and all approvals after the change has been implemented.**


### Approval Matrix

|	**Approval Type**	|	**Description**	|	**Standard**	|	**Comprehensive**	|	**Emergency**
|	-----	|	-----	|	-----	|	-----	|	-----
|	**Peer Review**	|	Peer Reviews are performed by a peer of the change Requestor or Developer and are intended to identify any potential issues with the planned change or change process. **Note:** The peer review process was established to mitigate the risk of the lack of segregation of duties between developer and implementer. The review provides comfort that changes to the production environment are valid.	|	Yes	|	Yes	|	Yes
|	**Impacted Team(s) Management approval**	|	Approval by Management that is responsible for the particular system	|	Yes	|	Yes	|	Yes
|	**Business Approval**	|	Approval by Impacted Team(s) Management approval that is responsible for the particular system or is impacted by the change.	|	No	|	Yes	|	Yes
|	**Head of IT, Business**	|	The Head of IT must approve all changes made during blackout periods	|	No	|	Yes	|	Yes* |

* (*) Refer to Emergency Change section under [Change Request Types](https://about.gitlab.com/handbook/business-technology/change-management/#change-request-types) for approval requirement details. 
|	**Head of IT, Business**	|	The Head of IT must approve all changes made during blackout periods	|	No	|	Yes	|	Yes |
